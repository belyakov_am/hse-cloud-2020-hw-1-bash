#!/usr/bin/env bash

readarray -t countries

ans=(${countries[*]/#[A-Z]/.})

echo "${ans[*]}"