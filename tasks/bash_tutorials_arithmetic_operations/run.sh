#!/usr/bin/env bash

read expression
ans=$(echo "$expression" | bc -l)
printf %.3f "$ans"
