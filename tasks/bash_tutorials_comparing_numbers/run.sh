#!/usr/bin/env bash

read x
read y

if [ "$x" -gt "$y" ]; then
    printf "X is greater than Y"
elif [ "$x" -lt "$y" ]; then
    printf "X is less than Y"
else
    printf "X is equal to Y"
fi
