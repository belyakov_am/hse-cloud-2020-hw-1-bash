#!/usr/bin/env bash

read chr
lowered=$(echo "$chr" | awk '{print tolower($0)}')

if [ "$lowered" = "y" ]; then
    printf "YES"
elif [ "$lowered" = "n" ]; then
    printf "NO"
fi
