#!/usr/bin/env bash

read a
read b
read c

if [ $a == $b ] && [ $b == $c ] && [ $a == $c ]; then
  printf "EQUILATERAL"
elif [ $a == $b ] || [ $b == $c ] || [ $a == $c ]; then
  printf "ISOSCELES"
else
  printf "SCALENE"
fi
