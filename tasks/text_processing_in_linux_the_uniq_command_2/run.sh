#!/usr/bin/env bash

uniq -c | awk '{ print $1, $2 }' | head -c -2
