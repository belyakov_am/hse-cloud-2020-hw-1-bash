#!/usr/bin/env bash

read n
sum_=0

for i in $(seq 1 $n)
do
    read num
    let "sum_+=$num"
done

average=$(echo "$sum_ / $n" | bc -l)
printf "%.3f\n" "$average"
