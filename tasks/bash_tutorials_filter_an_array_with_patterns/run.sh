#!/usr/bin/env bash

readarray -t countries

ans=(${countries[*]/*[aA]*/})

echo "${ans[*]}"
