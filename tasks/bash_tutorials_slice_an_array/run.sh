#!/usr/bin/env bash

readarray -t countries

echo "${countries[*]:3:5}"
